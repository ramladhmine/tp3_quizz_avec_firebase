﻿# tp3_quizz_avec_firebase

Le but de ce TP est de nous familiariser avec la Plateforme Firebase, ajout de projet Firebase, intégration avec une application flutter, création de base de données. Il consiste à utiliser la base de données Firebase Database pour stocker les questions ainsi que leurs réponses. Les questions sont catégorisées par thématique.


## Getting started

Quelques ressources pour vous aider à démarrer s'il s'agit de votre premier projet Flutter :

    • Flutter & Firebase App Build : https://www.youtube.com/playlist?list=PL4cUxeGkcC9j--TKIdkb3ISfRbJeJYQwC


