import 'package:firebase_quizz/data/dataproviders/quizzPageProvider.dart';
import 'package:firebase_quizz/data/models/question.dart';
import 'package:firebase_quizz/presentation/screens/finalPage.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';

part 'quizzPageState.dart';

class QuizzPageCubit extends Cubit<QuizzPageState> {

  List<Question> _questions=[] ;
  int _index=0 ;
  int _score=0;

  QuizzPageCubit(this._questions,this._index,this._score) : super(QuestionsQuizInitial(_questions,0,0));


  int get score => _score;

  set score(int value) {
    _score = value;
  }

  int get index => _index;

  set index(int value) {
    _index = value;
  }

  List<Question> get questions => _questions;

  set questions(List<Question> value) {
    _questions = value;
  }

  void nextQuestion(BuildContext context) {
    if (index < questions.length - 1) {
      index++;
    } else {
      Navigator.pushReplacement(
          context,
          MaterialPageRoute(
              builder: (context) => FinalPage(
                  score: _score
              )));
    }
    emit(QuestionsQuizInitial(questions, index, score));
  }


  void checkAnswer(bool userChoice, BuildContext context){
    if (questions[index].isCorrect==userChoice) {
      _score++ ;
      nextQuestion(context);
    } else {
      _score--;
      nextQuestion(context);
    }
    emit(QuestionsQuizInitial(questions, index, score));
  }


  void restart(){
    index =0;
    score=0;
    _questions = DataProvider.getAllQuestions();
    emit(QuestionsQuizInitial(questions, index, score));
  }



}