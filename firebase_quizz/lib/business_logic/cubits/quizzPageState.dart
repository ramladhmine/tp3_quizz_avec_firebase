part of 'quizzPageCubit.dart';

@immutable
abstract class QuizzPageState {}

class QuestionsQuizInitial extends QuizzPageState {
  List<Question> questions ;
  int index;
  int score;
  QuestionsQuizInitial(this.questions, this.index, this.score);

}
