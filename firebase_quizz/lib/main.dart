import 'package:firebase_quizz/data/dataproviders/quizzPageProvider.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:firebase_quizz/presentation/screens/homePage.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:firebase_quizz/business_logic/cubits/quizzPageCubit.dart';

void main() async{
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());

}
class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
        create: (_) => QuizzPageCubit(DataProvider.getAllQuestions(),0,0),
        child: BlocBuilder<QuizzPageCubit, QuizzPageState >(
            builder: (_, theme) {
              return MaterialApp(
                debugShowCheckedModeBanner: true,
                theme: ThemeData(
                  primarySwatch: Colors.blueGrey,
                ),
                home: HomePage(),
              );
            }
        )
    );

  }
}


