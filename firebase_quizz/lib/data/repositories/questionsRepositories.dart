import 'package:firebase_quizz/business_logic/cubits/quizzPageCubit.dart';
import 'package:firebase_quizz/data/models/question.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:provider/src/provider.dart';

class QuestionsRepository {
  static List<Question> questions = List.empty(growable: true);


  static Future<void> getAllQuestions() async {
    questions=[];
    QuerySnapshot query = await FirebaseFirestore.instance.collection('questions').get();
    List<QueryDocumentSnapshot> docs = query.docs;
    for (var doc in docs) {
      if (doc.data() != null) {
        var data = doc.data() as Map<String, dynamic>;
        questions.add(Question(
            question : data['question'], isCorrect : data['isCorrect'], thematic : data['thematic']));
      }
    }
  }

  static Future<void> addQuestions(Question question, BuildContext context) {
    return FirebaseFirestore.instance.collection('questions')
        .add({
      'question': question.question,
      'isCorrect': question.isCorrect,
      'thematic' : question.thematic
    }).then((value) => context.read<QuizzPageCubit>().restart())
        .catchError((error) => print("Failed to add user: $error"));
  }


}