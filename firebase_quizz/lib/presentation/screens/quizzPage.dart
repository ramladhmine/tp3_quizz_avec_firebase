import 'package:firebase_quizz/business_logic/cubits/quizzPageCubit.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:provider/src/provider.dart';


class AddQuestionQuizz extends StatefulWidget {
  @override
  _AddQuestionQuizzState createState() => _AddQuestionQuizzState();
}

class _AddQuestionQuizzState extends State<AddQuestionQuizz>
    with SingleTickerProviderStateMixin {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blueGrey,
        title: Text("Questions/Réponses"
            ,style: TextStyle(color: Colors.white, fontSize: 25)
        ),
        centerTitle: true,
      ),
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 30, vertical: 30),
        width: MediaQuery.of(context).size.width,
        decoration: new BoxDecoration(color: Colors.blueGrey),

        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              margin: const EdgeInsets.all(15),
              height: 180,
              alignment: Alignment.center,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage('assets/laFrancePhoto.png'),
                    fit: BoxFit.fill,
                  ),
            ),),
            Container(
              margin: const EdgeInsets.all(20),
              alignment: Alignment.center,
              child: Text(
                "Son thématique : " + context.read<QuizzPageCubit>().questions[context.read<QuizzPageCubit>().index].thematic,
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 18,
                    fontWeight: FontWeight.w500),
              ),
            ),
            SizedBox(height: 1),
            Container(
              margin: const EdgeInsets.all(20),
              height: 100,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  border: Border.all(
                    color: Colors.white12,
                  ),
                  borderRadius: const BorderRadius.all(Radius.circular(10))),
              child: Text(
                context.read<QuizzPageCubit>().questions[context.read<QuizzPageCubit>().index].question +"?",
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 20,
                    fontWeight: FontWeight.w500),
              ),
            ),
            SizedBox(height: 1),
            Container (
              padding: EdgeInsets.symmetric(vertical: 40),
              child : Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Expanded(
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          elevation: 5,
                          primary: Colors.green,
                        ),
                        onPressed: () {
                          context.read<QuizzPageCubit>().checkAnswer(true, context);
                        },
                        child: Text(
                          "VRAI",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 17,
                              fontWeight: FontWeight.w400),
                          textAlign: TextAlign.center,
                        ),
                      )),

                  const SizedBox(
                    width: 15,
                  ),

                  Expanded(
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          elevation: 5,
                          primary: Colors.red,
                        ),
                        onPressed: () {
                          context.read<QuizzPageCubit>().checkAnswer(false, context);
                        },
                        child: Text(
                          "FAUX",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 17,
                              fontWeight: FontWeight.w400),
                          textAlign: TextAlign.center,
                        ),
                      ),
                  ),
                  const SizedBox(
                    width: 20,
                  ),

                  Expanded(
                      child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            elevation: 15,
                            primary: Colors.blueGrey,
                          ),
                        onPressed: () {
                            setState(() {
                            context.read<QuizzPageCubit>().nextQuestion(context);
                          });},
                        child: Container(
                          padding: EdgeInsets.symmetric(vertical: 10),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10)),
                          child: const Icon(Icons.arrow_forward),
                        ),)
                  ),
                  const SizedBox(
                    width: 20,
                  ),


                ],
              ),
            )
          ],
        ),
      ),
    );
  }
  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }
}
