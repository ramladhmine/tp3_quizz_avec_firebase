import 'package:firebase_quizz/business_logic/cubits/quizzPageCubit.dart';
import 'package:firebase_quizz/data/dataproviders/quizzPageProvider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:firebase_quizz/presentation/screens/homePage.dart';
import 'package:provider/src/provider.dart';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';



class AddQuestionPage extends StatefulWidget{
  @override
  _AddQuestionPageState createState() => _AddQuestionPageState();
}

class _AddQuestionPageState extends State<AddQuestionPage> {
  TextEditingController valText = TextEditingController();
  TextEditingController valIsCorrect =  TextEditingController();
  TextEditingController valThematic =  TextEditingController();

  int nbCard = 1;
  final nomcategorie = TextEditingController();
  List<XFile?> images = [];
  List<TextEditingController> listquestion = [];
  final ImagePicker picker = ImagePicker();
  int index=0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: const Center(
            child: Text("Ajouter une question au quiz"),
          )
      ),
      backgroundColor: Colors.blueGrey,
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 30,vertical: 50),
        width: MediaQuery.of(context).size.width,
        child: SingleChildScrollView(
             child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    TextFormField(
                           controller: valText,
                          autofocus: false,
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 20
                          ),
                          decoration: const InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            hintText: 'Entrer la question',
                          ),
                        ),
                        SizedBox(height: 30),
                    TextFormField(
                      controller: valIsCorrect,
                      autofocus: false,
                      style: const TextStyle(
                          color: Colors.black,
                          fontSize: 20
                      ),
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: Colors.white,
                        hintText: 'vrai/faux',
                      ),
                    ),
                    SizedBox(height: 30),
                    TextFormField(
                      controller: valThematic,
                      autofocus: false,
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 20
                      ),
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: Colors.white,
                        hintText: 'Entrer le thématique',
                      ),
                    ),
                    SizedBox(height: 30),
                    GestureDetector(
                        onTap: () {
                          setState(() async {
                            var img = await picker.pickImage(source: ImageSource.gallery);
                            setState(() {
                              images[index] = img;});});},
                        child: Container(
                          padding: EdgeInsets.symmetric(vertical: 15, horizontal: 93),
                          child: Text("Ajouter une image",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 20),

                          ),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              color: Colors.black12),
                        ),
                    ),

                    SizedBox(height: 80),
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: 15),
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: <Widget>[
                            Expanded(
                                child: GestureDetector(
                                  onTap: () {
                                    DataProvider.addQuestion(valText.text, valIsCorrect.text, valThematic.text, context);
                                    context.read<QuizzPageCubit>().restart();
                                    Navigator.pushReplacement(context,
                                        MaterialPageRoute(builder: (context) => HomePage()));
                                  },
                                  child: Container(
                                    padding: EdgeInsets.symmetric(vertical: 10, horizontal: 30),
                                    child: Text(
                                      "Ajouter",
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 20,
                                          fontWeight: FontWeight.w500),
                                      textAlign: TextAlign.center,

                                    ),
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(15),
                                        color: Colors.white),
                                  ),)),
                            SizedBox(width: 50),
                            Expanded(
                              child: GestureDetector(
                                onTap: () {
                                  context.read<QuizzPageCubit>().restart();
                                  Navigator.pushReplacement(context,
                                      MaterialPageRoute(builder: (context) => HomePage()));
                                },
                                child: Container(
                                  padding: EdgeInsets.symmetric(vertical: 10, horizontal: 35),
                                  child: Text(
                                    "Retour",
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 20,
                                        fontWeight: FontWeight.w500),
                                    textAlign: TextAlign.center,

                                  ),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(15),
                                      color: Colors.white),
                                ),
                              ),),
                          ]
                      ),
                    ),






                  ],
             ),
        ),
      ),
    );}
}