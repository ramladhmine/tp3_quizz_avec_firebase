import 'package:firebase_quizz/presentation/screens/addQuestionQuizz.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:firebase_quizz/presentation/screens/quizzPage.dart';


class HomePage extends StatefulWidget {

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Center(
         child: Text("Quizz"),
        )
      ),
      backgroundColor: Colors.blueGrey,
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 30),
        width: MediaQuery.of(context).size.width,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            GestureDetector(
              onTap: (){
                Navigator.pushReplacement(context, MaterialPageRoute(
                    builder: (context) => AddQuestionQuizz()
                ));
              },
              child: Container(
                padding: EdgeInsets.symmetric(vertical: 15,horizontal: 30),
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10)
                ),
                child: Text("Jouer à ce Quizz", style: TextStyle(
                    color: Colors.black,
                    fontSize: 20
                ),),
              ),
            ),
            SizedBox(
              height: 30,
            ),
            GestureDetector(
              onTap: (){
                Navigator.pushReplacement(context, MaterialPageRoute(
                    builder: (context) => AddQuestionPage()
                ));
              },
              child: Container(
                padding: EdgeInsets.symmetric(vertical: 15,horizontal: 30),
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10)
                ),
                child: Text("Ajouter une question au quizz ", style: TextStyle(
                    color: Colors.black,
                    fontSize: 20
                ),),
              ),
            ),

          ],
        ),
      ),
    );
  }
}