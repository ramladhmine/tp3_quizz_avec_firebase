import 'package:firebase_quizz/business_logic/cubits/quizzPageCubit.dart';
import 'package:firebase_quizz/presentation/screens/homePage.dart';
import 'package:flutter/material.dart';
import 'package:provider/src/provider.dart';

class FinalPage extends StatefulWidget {
  int score;
  FinalPage({required this.score});

  @override
  _FinalPageState createState() => _FinalPageState();
}

class _FinalPageState extends State<FinalPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blueGrey,
      body: Container(
        width: MediaQuery.of(context).size.width,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            SizedBox(
              height: 14,
            ),
            Text("Votre résultat est : ${widget.score} ",
              style: TextStyle(
                color: Colors.white,
                fontSize: 25,
                fontWeight: FontWeight.w500,
              ),
            ),
            SizedBox( height: 30 ),
            GestureDetector(
              onTap: () {
                context.read<QuizzPageCubit>().restart();
                Navigator.pushReplacement(context,
                    MaterialPageRoute(builder: (context) => HomePage()));
              },
              child: Container(
                padding: EdgeInsets.symmetric(vertical: 12, horizontal: 54),
                child: Text(
                  "Rejouez",
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 20,
                      fontWeight: FontWeight.w500),
                ),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Colors.white),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
